import { View, Text, TextInput, TouchableOpacity, ImageBackground, Alert } from 'react-native'
import Service from '../Config/Service';
import { useContext, useState } from 'react';
import { AuthContext } from '../utils/context/AuthContext';
export default function LoginScreen({ navigation }) {

    const {login} = useContext(AuthContext);
    const [ten_dang_nhap, setTenDangNhap] = useState('');
    const [mat_khau, setMatKhau] = useState('');

    return (
        <ImageBackground source={require('../../assets/backgroudLogin.jpg')} resizeMode='stretch' style={{ width: '100%', height: '100%' }}>
            <View className='justify-center flex-1'>
                <Text className='text-center mt-3 text-2xl font-bold text-gray-50'>
                    Đăng nhập 
                </Text>
                <View className='mt-5 mx-10'>
                    <View>
                        <Text className='text-gray-50'>Tên đăng nhập:</Text>
                        <TextInput
                            placeholder='nhập tên đăng nhập'
                            className='border border-dotted p-2 text-gray-50 border-gray-200 mt-1 w-full'
                            onChange={e=> setTenDangNhap(e.nativeEvent.text)}
                       />
                    </View>
                    <View className='mt-3'>
                        <Text className='text-gray-50'>Mật khẩu:</Text>
                        <TextInput
                            secureTextEntry
                            placeholder='Nhập mật khẩu'
                            className='border text-gray-50 border-dotted p-2 border-gray-200 mt-1'
                            onChange={e=> setMatKhau(e.nativeEvent.text)}
                        />
                    </View>
                    <View className="items-center justify-center  mt-4">
                        <TouchableOpacity className='bg-orange-300 p-3 mt-4 rounded-3xl  w-[250]' onPress={() => login(ten_dang_nhap, mat_khau)}>
                            <Text className='text-center text-base text-white '>Đăng nhập</Text>
                        </TouchableOpacity>
                        <Text className='text-center font-normal text-white text-base mt-3'>
                            OR
                        </Text>
                        <View className='mt-4'>
                            <TouchableOpacity className='flex flex-row items-center justify-center p-3 bg-orange-300 rounded-3xl  w-[250]' onPress={() => navigation.navigate('register')}>
                                <Text className='text-white mx-2 text-sm'>Đăng ký</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </ImageBackground>
    )

}
