import { StatusBar } from 'expo-status-bar';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Navigations from './Apps/Navigation/Navigations';
import React, { useEffect, useState } from 'react';
import { AuthProvider } from './Apps/utils/context/AuthContext';
export default function App() {
  const Stack = createNativeStackNavigator();
  return (
    <AuthProvider>
      <NavigationContainer>
        <StatusBar style='auto' />
        <Navigations />
      </NavigationContainer>
    </AuthProvider>
  );
}
