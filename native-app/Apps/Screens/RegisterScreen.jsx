import {View, Text, TextInput, TouchableOpacity, ImageBackground } from 'react-native'
import React from 'react'

export default function RegisterScreen({navigation}) {
  return (
    <ImageBackground source={require('../../assets/backgroudLogin.jpg')} resizeMode='stretch' style={{ width: '100%', height: '100%',  }}>
            <View className='justify-center flex-1'>
                <Text className='text-center mt-3 text-2xl font-bold text-gray-50'>
                    Đăng ký
                </Text>
                <View className='mt-5 mx-10'>
                    <View>
                        <Text className='text-gray-50'>Tên đăng nhập:</Text>
                        <TextInput
                            placeholder='nhập tên đăng nhập'
                            className='border border-dotted p-2 text-gray-50 border-gray-200 mt-1 w-full'
                        />
                    </View>
                    <View className='mt-3'>
                        <Text className='text-gray-50'>Mật khẩu:</Text>
                        <TextInput
                            secureTextEntry
                            placeholder='Nhập mật khẩu'
                            className='border text-gray-50 border-dotted p-2 border-gray-200 mt-1'
                        />
                    </View>
                    <View className='mt-3'>
                        <Text className='text-gray-50'>Email:</Text>
                        <TextInput
                            secureTextEntry
                            placeholder='Nhập email'
                            className='border text-gray-50 border-dotted p-2 border-gray-200 mt-1'
                        />
                    </View>
                    <View className="items-center justify-center  mt-4">
                        <TouchableOpacity className='bg-orange-300 p-3 mt-4 rounded-3xl  w-[250]'>
                            <Text className='text-center text-base text-white '> Đăng ký</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
    </ImageBackground>
  )
}