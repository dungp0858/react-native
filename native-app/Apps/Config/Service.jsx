import React from 'react';
import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
const API_URL = "http://192.168.10.117:5001/";

const getData=(url, data)=>{
    return axios.get(API_URL+url).then(reponse=>{
        return reponse;
    }).catch(error => console.log(error));
};
const postData=(url, data)=>{
    return axios.post(API_URL+url,data).then(reponse=>{
        return reponse;
    }).catch(error => console.log(error));
};


const service = {
    getData,
    postData
}
export default service;