import { View, Text, ActivityIndicator } from 'react-native'
import React, { useContext } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../Screens/LoginScreen';
import RegisterScreen from '../Screens/RegisterScreen';
import { NavigationContainer } from '@react-navigation/native';
import { HomeScreen } from '../Screens/HomeScreen';
import { AuthContext } from '../utils/context/AuthContext';
export default function Navigations() {
    const {isLoading, userToken} = useContext(AuthContext);
    const Stack = createNativeStackNavigator();
    if(isLoading){
        <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
            <ActivityIndicator size={'large'}/>
        </View>
    }

    return (
        <Stack.Navigator initialRouteName="Login" >
            <Stack.Screen name="Login" component={LoginScreen} options={{headerShown:false}} />
            <Stack.Screen name="register" component={RegisterScreen} options={{ title:"Đăng ký"}} />
            <Stack.Screen name="home" component={HomeScreen} options={{headerShown:false}}/>
        </Stack.Navigator >
     )
}