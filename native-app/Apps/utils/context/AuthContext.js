import React, {createContext, useState} from "react";
import Service from '../../Config/Service';
import { View, Text, TextInput, TouchableOpacity, ImageBackground, Alert } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
export const AuthContext = createContext();
export const AuthProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [userToken, setuserToken] = useState(null);

    const login = (ten_dang_nhap,mat_khau) =>{
        Service.postData('api/login', { 
            ten_dang_nhap:ten_dang_nhap,
            mat_khau: mat_khau
        }).then(async (response)=>{
            const data = response.data;
            if(data.Code == -1){
                Alert.alert(data.message);
            }else if(data.Code == 1){
                await AsyncStorage.setItem('token', data.data.Accesstoken);
                Alert.alert("Đăng nhập thành công");
                setuserToken(data.data.Accesstoken);
            }
        });
    }

    return(
        <AuthContext.Provider value={{login, userToken}}>
            {children}
        </AuthContext.Provider>
    );
}